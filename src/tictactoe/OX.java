/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.*;

/**
 *
 * @author PlugPC
 */
public class OX {

    public static char[][] table = {{' ', '1', '2', '3'},
    {'1', '-', '-', '-'},
    {'2', '-', '-', '-'},
    {'3', '-', '-', '-'}};
    public static int col;
    public static int row;
    private static int turn;

    public static void showTurn() {
        if (turn % 2 == 1) {
            System.out.println("X turn");
        } else {
            System.out.println("O turn");
        }
    }

    public static void input() {
        Scanner kb = new Scanner(System.in);
        System.out.println("input row col: ");

        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    public static boolean checkWin() {
        if (table[1][1] == 'X' && table[2][2] == 'X' && table[3][3] == 'X'
                    || table[1][3] == 'X' && table[2][2] == 'X' && table[3][1] == 'X'
                    || table[1][1] == 'X' && table[1][2] == 'X' && table[1][3] == 'X'
                    || table[2][1] == 'X' && table[2][2] == 'X' && table[2][3] == 'X'
                    || table[3][1] == 'X' && table[3][2] == 'X' && table[3][3] == 'X'
                    || table[1][1] == 'X' && table[2][1] == 'X' && table[3][1] == 'X'
                    || table[1][2] == 'X' && table[2][2] == 'X' && table[3][2] == 'X'
                    || table[1][3] == 'X' && table[2][3] == 'X' && table[3][3] == 'X'){
            System.out.println("X won!!");
            return true;
        }
        else if(table[1][1] == 'O' && table[2][2] == 'O' && table[3][3] == 'O'
                    || table[1][3] == 'O' && table[2][2] == 'O' && table[3][1] == 'O'
                    || table[1][1] == 'O' && table[1][2] == 'O' && table[1][3] == 'O'
                    || table[2][1] == 'O' && table[2][2] == 'O' && table[2][3] == 'O'
                    || table[3][1] == 'O' && table[3][2] == 'O' && table[3][3] == 'O'
                    || table[1][1] == 'O' && table[2][1] == 'O' && table[3][1] == 'O'
                    || table[1][2] == 'O' && table[2][2] == 'O' && table[3][2] == 'O'
                    || table[1][3] == 'O' && table[2][3] == 'O' && table[3][3] == 'O'){
            System.out.println("O won!!");
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean checkDraw() {
        if(turn == 9){
            System.out.println("Draw!!");
            return true;
        }
        else{
            return false;
        }
    }

    public static void main(String[] args) {
        turn = 1;
        for (;;) {
            showTurn();
            input();
            if (row > 3 || col > 3) {
                System.out.println("Error your number should be less than 3");
                continue;
            }
            else if(row < 1 || col < 1){
                System.out.println("Error your number should be much for 0");
                continue;
            }else {
                
                if (turn % 2 == 1) {
                    table[row][col] = 'X';
                } else {
                    table[row][col] = 'O';
                }
                showTable();
            }
            if(checkWin()){
                break;
            }
            if(checkDraw()){
                break;
            }
            turn++;
        }

    }

}
